using Godot;
using Extensions;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class Hazard : Node2D
	{
		[Export]
		public float damage;
		[Export]
		public float radius;
		[Export]
		public Area2D area;
		public static List<Hazard> instances = new List<Hazard>();

		public override void _Ready ()
		{
			area.BodyEntered += OnBodyEntered;
			instances.Add(this);
			TreeExiting += OnDestroy;
		}
		
		public virtual void OnDestroy ()
		{
			area.BodyEntered -= OnBodyEntered;
			instances.Remove(this);
		}
		
		public virtual void OnBodyEntered (Node2D node)
		{
			IDestructable destructable = node.GetParentInterface<IDestructable>();
			if (destructable != null)
				destructable.TakeDamage (damage);
		}
	}
}
