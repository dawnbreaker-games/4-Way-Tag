using Godot;
using System;

namespace FightRoom
{
	public partial class _Camera2D : Camera2D
	{
		[Export]
		public Vector2 viewSize;
		[Export]
		public float zoomRate;
		[Export]
		public float zoomMultiplier = 1;
		public FloatRange zoomRange = new FloatRange(1, 1);
		static float deltaTime;
		
		public override void _Ready ()
		{
			float[] zoomRangeComponents = (float[]) GetMeta("zoomRange", zoomRange.ToArray());
			zoomRange = (FloatRange) FloatRange.FromArray(zoomRangeComponents);
			GetWindow().Unresizable = true;
			GetWindow().Borderless = true;
			GetWindow().Size = GetBestFit();
			GetWindow().Position = Vector2I.Zero;
		}
		
		public override void _Process (double deltaTime)
		{
			_Camera2D.deltaTime = (float) deltaTime;
			Zoom = Vector2.One * Mathf.Min(GetWindow().Size.X / viewSize.X, GetWindow().Size.Y * viewSize.Y) * zoomMultiplier;
		}
		
		public override void _UnhandledInput (InputEvent inputEvent)
		{
			InputEventMouseButton mouseButtonEvent = inputEvent as InputEventMouseButton;
			if (mouseButtonEvent != null)
			{
				if (mouseButtonEvent.IsPressed())
				{
					int zoomDirection = 0;
					if (mouseButtonEvent.ButtonIndex == MouseButton.WheelUp)
						zoomDirection = 1;
					else if (mouseButtonEvent.ButtonIndex == MouseButton.WheelDown)
						zoomDirection = -1;
					zoomMultiplier = zoomRange.Clamp(zoomMultiplier + zoomDirection * zoomRate * deltaTime);
				}
			}
		}
		
		Vector2I GetBestFit ()
		{
			float viewSizeAspectRatio = viewSize.X / viewSize.Y;
			float screenAspectRatio = (float) DisplayServer.ScreenGetSize().X / DisplayServer.ScreenGetSize().Y;
			return (Vector2I) (DisplayServer.ScreenGetSize() * new Vector2(Mathf.Min(1, viewSizeAspectRatio / screenAspectRatio), Mathf.Min(1, screenAspectRatio / viewSizeAspectRatio)));
		}
	}
}
