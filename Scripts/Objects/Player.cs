using Godot;
using System;
using Extensions;

namespace FightRoom
{
	public partial class Player : Node2D, IDestructable
	{
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public uint MaxHp
		{
			get
			{
				return maxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		[Export]
		public uint maxHp;
		[Export]
		public CharacterBody2D characterBody;
		[Export]
		public float moveSpeed = 10;
		float hp;
		bool dead;
		
		public override void _Ready ()
		{
			hp = maxHp;
		}
		
		public override void _PhysicsProcess (double deltaTime)
		{
			Vector2 input = Input.GetVector("Move left", "Move right", "Move up", "Move down");
			characterBody.Velocity = input * moveSpeed;
			if (input != Vector2.Zero)
				characterBody.RotationDegrees = VectorExtensions.GetFacingAngle(input);
			characterBody.MoveAndSlide();
		}
		
		public void TakeDamage (float amount)
		{
			if (dead)
				return;
			hp = Mathf.Clamp(hp - amount, 0, maxHp);
			if (hp == 0)
				Death ();
		}
		
		public void Death ()
		{
			dead = true;
		}
	}
}
